# Visualizer Backend

# Pre-requisites
* Python is installed
* Node is installed
* You have a database copy of tunes backend

## Running the application

1. Clone this repository
2. Install dependecies
  ```sh
  python setup.py
  ```
3. Update instance/config.py
4. Install node depedencies at ext/image_bg_generator
5. Install Phantomjs
6. Install ffmpeg
7. Install these python dependencies
    * sh
    * wave
    * numpy
    * pyaudio
    * pydub
    * PIL
8. Run the app
  ```sh
  python run.py
  ```
