# Import global context
from flask import request

from flask import send_from_directory

# Import flask dependencies
from flask import Blueprint

# Import app-based dependencies
from app import app, video
from util import utils

# Import core libraries
from lib.decorators import make_response

import os.path

# Define the blueprint: 'video', set its url prefix: app.url/video
mod_video = Blueprint('video', __name__)


# Declare all the routes

@mod_video.route('/<video_filename>', methods=['GET'])
@make_response
def get_video(res, video_filename):
    return send_from_directory(app.config['BASE_DIR'] + '/../public', video_filename, as_attachment=True)