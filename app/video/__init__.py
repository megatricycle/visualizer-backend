# Import app-based dependencies
from app import app
from util import utils

# Import core libraries
from lib import database
from lib.error_handler import FailedRequest



config = app.config
db = app.db
