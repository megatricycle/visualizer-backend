# Import global context
from flask import request

# Import flask dependencies
from flask import Blueprint

# Import app-based dependencies
from app import track
from util import utils

# Import core libraries
from lib.decorators import make_response

import json

# Define the blueprint: 'track', set its url prefix: app.url/track
mod_track = Blueprint('track', __name__)


# Declare all the routes

@mod_track.route('/', methods=['GET'])
@make_response
def get_track(res):

    args = utils.get_data(['index'], [], request.args)

    args['index'] = int(args['index'])

    result = track.get_track(args)
    
    return res.send(result)

@mod_track.route('/generate', methods=['POST'])
@make_response
def generate_video(res):
    # @TODO: Validation

    items = request.values.to_dict()

    items = json.loads(items['json'])

    # args = utils.get_data(['title', 'artist', 'file_name', 'rock', 'classic', 'acoustic', 'rnb'], ['bg_filename'], items)

    args = {
        'title': items['title'],
        'artist': items['artist'],
        'bg_filename': items['bg_filename'].replace(' ', '+') if items['bg_filename'] else '',
        'file_name': items['file_name'],
        'genres': {
            'rock': items['rock'],
            'classic': items['classic'],
            'acoustic': items['acoustic'],
            'rnb': items['rnb']
        },
        'v_mode': items['v_mode']
    }

    result = track.generate_video(args)

    return res.send(result)