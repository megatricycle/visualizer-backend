# Import app-based dependencies
from app import app
from util import utils

# Import core libraries
from lib import database
from lib.error_handler import FailedRequest

from ext import visualize

from uuid import uuid4

config = app.config
db = app.db


def get_track(args):
    print(args)

    data = database.get(
        db.app_db, 'SELECT tracks.track_id, tracks.title, tracks.file_path, artists.name, artists.picture FROM tracks, artists WHERE tracks.artist_id = artists.artist_id ORDER BY tracks.title ASC LIMIT 50 OFFSET :index', args)

    if not data:
        raise FailedRequest('Track not found')

    return data

def generate_video(options):
    id = str(uuid4())

    url = visualize(id, options)

    return {
        'url': url
    }
