import urllib.request
import shutil
import os
from subprocess import call
from ext.data_generator import analyze
from ext.image_bar_generator import generate_images

MUSIC_HOST = 'http://d1gyn9a1opa35u.cloudfront.net/tracks/' 

def visualize(id, options):
    dir = os.path.dirname(os.path.realpath(__file__))

    # make folder for this session
    # shutil.rmtree(dir + '/shared/' + id)
    os.mkdir(dir + '/shared/' + id)

    # generate bg image
    os.chdir(dir + '/image_bg_generator')

    # print base64 into a temporary file
    bg_file = open(dir + '/shared/{}/bg_data.txt'.format(id), 'w')
    bg_file.write(options['bg_filename'])
    bg_file.close()

    html_command = ['npm', 'run', '-s', 'build-html', '--', options['title'], options['artist'], 'true' if options['genres']['rock'] else 'false', 'true' if options['genres']['classic'] else 'false', 'true' if options['genres']['rnb'] else 'false', 'true' if options['genres']['acoustic'] else 'false', id]

    call(html_command) 

    screenshot_command = ['npm', '-s', 'start', id]
    call(screenshot_command)

    os.chdir('..')

    # # @TODO: Thread these two?

    # download from host
    with urllib.request.urlopen(MUSIC_HOST + options['file_name']) as response, open(dir + '/shared/' + id + '/sound.mp3', 'wb') as out_file:
        shutil.copyfileobj(response, out_file)

    # # generate frequency data
    frequency_data = analyze(id, options['v_mode'])

    # # generate visualization images
    generate_images(id, frequency_data, options['v_mode'])

    # # convert to video
    video_command = ['ffmpeg', '-r', '24', '-f', 'image2', '-s', '1280x720', '-i', dir + '/shared/{}/frames/%07d.png'.format(id), '-i', dir + '/shared/{}/sound.mp3'.format(id), '-vcodec', 'libx264', '-acodec', 'copy', '-crf', '25', '-pix_fmt', 'yuv420p', dir + '/../public/{}.mp4'.format(id)]
    call(video_command)

    # delete shared
    shutil.rmtree(dir + '/shared/{}'.format(id), ignore_errors = True)

    return '/video/{}.mp4'.format(id)