import os
from ext.image_bar_generator.draw_image import draw_image_bar, draw_image_pulse

def generate_images(id, frequency_data_bin, v_mode):
    dir = os.path.dirname(os.path.realpath(__file__))

    os.mkdir(dir + '/../shared/{}/frames'.format(id))

    for i in range(0, len(frequency_data_bin)):
        if v_mode == 'bar':
            draw_image_bar(frequency_data_bin[i], id, i, i/len(frequency_data_bin))
        else:
            draw_image_pulse(frequency_data_bin[i], id, i)