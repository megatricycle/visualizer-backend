from PIL import Image, ImageDraw
from ext.image_bar_generator.util import percentToOpacityBar, percentToOpacityPulse
import os

IMAGE_WIDTH = 1280
IMAGE_HEIGHT = 720
BAR_WIDTH = 30
BAR_MARGIN = 10
BAR_MAX_HEIGHT = 450
BAR_BASE_Y_COORDINATE = 490
FREQUENCY_MAX = 1000
DIVIDER_LENGTH = 500
DIVIDER_Y_COORDINATE = 500
CIRCLE_MAX_DIAMETER = 600
CIRCLE_OUTER_WIDTH = 2
CIRCLE_2ND_WIDTH = 3
CIRCLE_2ND_MARGIN = 15
CIRCLE_INNER_WIDTH = 5
CIRCLE_INNER_MARGIN = 40

def draw_image_bar(frequency_bin_data, id, n, percent_duration):

    frequency_bin_data = list(reversed(frequency_bin_data))

    dir = os.path.dirname(os.path.realpath(__file__))

    for i in range(0, len(frequency_bin_data)):
        frequency_bin_data[i] = frequency_bin_data[i] / 3000
        if frequency_bin_data[i] > 1:
            frequency_bin_data[i] = 1

    base = Image.open(dir + '/../../shared/' + id + '/images/bg.jpg').convert('RGBA')

    draw = Image.new('RGBA', base.size, (0,0,0,0))

    d = ImageDraw.Draw(draw)

    # draw bottom divider
    d.line(((IMAGE_WIDTH / 2) - (DIVIDER_LENGTH / 2), DIVIDER_Y_COORDINATE, (IMAGE_WIDTH / 2) - (DIVIDER_LENGTH / 2) + DIVIDER_LENGTH, DIVIDER_Y_COORDINATE), fill=(255, 255, 255, 40), width=5)

    # draw duration
    d.line(((IMAGE_WIDTH / 2) - (DIVIDER_LENGTH / 2), DIVIDER_Y_COORDINATE, (IMAGE_WIDTH / 2) - (DIVIDER_LENGTH / 2) + DIVIDER_LENGTH * percent_duration, DIVIDER_Y_COORDINATE), fill=(255, 255, 255, 200), width=5)

    def drawBar(intensity, i):
        d.line((IMAGE_WIDTH / 2 - (i * (BAR_MARGIN + BAR_WIDTH) + BAR_MARGIN*2), (BAR_BASE_Y_COORDINATE - BAR_MAX_HEIGHT*intensity), IMAGE_WIDTH / 2 - (i * (BAR_MARGIN + BAR_WIDTH) + BAR_MARGIN*2), BAR_BASE_Y_COORDINATE), fill=(255, 255, 255, percentToOpacityBar(intensity)), width=BAR_WIDTH)

    # draw bars
    for i in range(-6, 6):
        drawBar(frequency_bin_data[i + 6], i)

    out = Image.alpha_composite(base, draw)

    out.save(dir + '/../../shared/{}/frames/{}.png'.format(id, '{0:07d}'.format(n)))


def draw_image_pulse(frequency_bin_data, id, n):

    dir = os.path.dirname(os.path.realpath(__file__))

    frequency_bin_data = frequency_bin_data / FREQUENCY_MAX

    base = Image.open(dir + '/../../shared/' + id + '/images/bg.jpg').convert('RGBA')

    draw = Image.new('RGBA', base.size, (0,0,0,0))

    d = ImageDraw.Draw(draw)

    # draw outer
    d.ellipse((IMAGE_WIDTH/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2, IMAGE_HEIGHT/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2, IMAGE_WIDTH/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2, IMAGE_HEIGHT/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2), fill=(255,255,255,percentToOpacityPulse(frequency_bin_data)))
    d.ellipse((IMAGE_WIDTH/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_OUTER_WIDTH, IMAGE_HEIGHT/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_OUTER_WIDTH, IMAGE_WIDTH/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_OUTER_WIDTH, IMAGE_HEIGHT/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_OUTER_WIDTH), fill=(0,0,0,0))

    # draw 2nd
    d.ellipse((
        IMAGE_WIDTH/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_2ND_MARGIN,
        IMAGE_HEIGHT/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_2ND_MARGIN,
        IMAGE_WIDTH/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_2ND_MARGIN,
        IMAGE_HEIGHT/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_2ND_MARGIN
    ), fill=(255,255,255,percentToOpacityPulse(frequency_bin_data)))
    d.ellipse((
        IMAGE_WIDTH/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_2ND_WIDTH + CIRCLE_2ND_MARGIN,
        IMAGE_HEIGHT/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_2ND_WIDTH + CIRCLE_2ND_MARGIN,
        IMAGE_WIDTH/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_2ND_WIDTH - CIRCLE_2ND_MARGIN,
        IMAGE_HEIGHT/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_2ND_WIDTH - CIRCLE_2ND_MARGIN
    ), fill=(0,0,0,0))

    # draw inner
    d.ellipse((
        IMAGE_WIDTH/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_INNER_MARGIN,
        IMAGE_HEIGHT/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_INNER_MARGIN,
        IMAGE_WIDTH/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_INNER_MARGIN,
        IMAGE_HEIGHT/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_INNER_MARGIN
    ), fill=(255,255,255,percentToOpacityPulse(frequency_bin_data)))
    d.ellipse((
        IMAGE_WIDTH/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_INNER_WIDTH + CIRCLE_INNER_MARGIN,
        IMAGE_HEIGHT/2 - CIRCLE_MAX_DIAMETER*frequency_bin_data/2 + CIRCLE_INNER_WIDTH + CIRCLE_INNER_MARGIN,
        IMAGE_WIDTH/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_INNER_WIDTH - CIRCLE_INNER_MARGIN,
        IMAGE_HEIGHT/2 + CIRCLE_MAX_DIAMETER*frequency_bin_data/2 - CIRCLE_INNER_WIDTH - CIRCLE_INNER_MARGIN
    ), fill=(0,0,0,0))

    out = Image.alpha_composite(base, draw)

    out.save(dir + '/../../shared/{}/frames/{}.png'.format(id, '{0:07d}'.format(n)))