import fs from 'fs-promise';
import renderComponent from './src/renderComponent';

if(process.argv.length != 9) {
    console.log('Usage: npm run -s build-html -- Title Artist isRock isClassic isRnb isAcoustic id')
    process.exit(0);
}

let args = {
    title: process.argv[2],
    artist: process.argv[3],
    rock: process.argv[4] === 'true',
    classic: process.argv[5] === 'true',
    rnb: process.argv[6] === 'true',
    acoustic: process.argv[7] === 'true',
    id: process.argv[8]
};

let data = {
    html: '',
    css: '',
    vendorCss: '',
    component: '',
    compiled: ''
};

fs.readFile(__dirname + '/../shared/' + args.id + '/bg_data.txt', { encoding: 'utf8' })
    .then((file) => {
        args.bg_image = file;

        return fs.readFile(__dirname + '/src/index.html', { encoding: 'utf8'}); 
    })
    .then((file) => {
        data.html = file;

        return fs.readFile(__dirname + '/src/style.css', { encoding: 'utf8'});
    })
    .then((file) => {
        data.css = file;

        return fs.readFile(__dirname + '/src/vendor.css', { encoding: 'utf8'});
    })
    .then((file) => {
        data.vendor = file;

        return Promise.resolve(renderComponent({
            title: args.title,
            artist: args.artist,
            bg_image: args.bg_image,
            genres: {
                rock: args.rock,
                rnb: args.rnb,
                classic: args.classic,
                acoustic: args.acoustic
            }
        }));
    })
    .then((component) => {
        data.component = component;

        let html = data.html;

        // place vendor files
        html = html.replace("<link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,500,700,900' rel='stylesheet' type='text/css'>", '<style>' + data.vendor + '</style>');
        html = html.replace('<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">', '');

        // place css
        html = html.replace('<link rel="stylesheet" type="text/css" href="style.css">', '<style>' + data.css + '</style>');

        // place component
        html = html.replace('${root}', data.component);

        // remove js
        html = html.replace('<script src="bundle.js"></script>', '');

        data.compiled = html;

        return fs.mkdir(__dirname + '/../shared/' + args.id + '/html');
    })
    .then(() => {
        return fs.writeFile(__dirname + '/../shared/' + args.id + '/html/page.html', data.compiled);
    })
    .catch((err) => {
        console.log(err);
    });