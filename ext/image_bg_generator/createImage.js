var system = require('system');
var id = system.args[1];
var fs = require('fs');

if (!id) {
    console.log('Error: ID required');
    phantom.exit();
}

var page = require('webpage').create();

page.viewportSize = { width: 1280, height: 720 };

page.open('../shared/' + id + '/html/page.html', function() {

    fs.makeDirectory('../shared/' + id + '/images')

    page.render('../shared/' + id + '/images/bg.jpg');

    phantom.exit();
})