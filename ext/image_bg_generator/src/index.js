import React from 'react';
import ReactDOM from 'react-dom';
import Component from './component';

ReactDOM.render(<Component
    bg_image=""
    genres={{
        rock: true,
        rnb: true,
        classic: false,
        acoustic: false
    }}
/>, document.getElementById('root'));