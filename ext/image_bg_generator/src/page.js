import React, { PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import ImageLinkedCamera from 'material-ui/svg-icons/image/linked-camera';
import ImageMusicNote from 'material-ui/svg-icons/image/music-note';
import ActionPanTool from 'material-ui/svg-icons/action/pan-tool';
import AVHearing from 'material-ui/svg-icons/av/hearing';
import ActionStars from 'material-ui/svg-icons/action/stars';

const V_ICON_SIZE = 175;
const GENRE_ICON_SIZE = 75;

const localStyles = {
    vIconStyle: {
        width: V_ICON_SIZE,
        height: V_ICON_SIZE
    },
    vIconButton: {
        width: V_ICON_SIZE * 2,
        height: V_ICON_SIZE * 2,
        padding: 30
    },
    genreIconStyle: {
        width: GENRE_ICON_SIZE,
        height: GENRE_ICON_SIZE
    },
    genreIconButton: {
        width: GENRE_ICON_SIZE * 1.5,
        height: GENRE_ICON_SIZE * 1.5,
        padding: 15
    }
};

class Page extends React.Component {
    render() {
        return (
            <div 
                className="wrapper"
                style={
                    this.props.bg_image ?
                    {
                        backgroundImage: 'url(' + this.props.bg_image + ')'
                    } :
                    {
                        backgroundColor: 'black'
                    }
                }
            >
                <div className="overlay"></div>

                <div className="textInputContainer">
                    <TextField
                        hintText="Title"
                        underlineShow={false}
                        value={this.props.title}
                        style={{
                            fontSize: '3em',
                            width: '100%',
                        }}
                        inputStyle={{
                            color: 'white',
                            opacity: 0.7,
                            height: '100% !important'
                        }}
                    />
                    <TextField
                        hintText="Artist"
                        underlineShow={false}
                        value={this.props.artist}
                        style={{
                            fontSize: '1.5em',
                            width: '100%'
                        }}
                        inputStyle={{
                            color: 'white',
                            opacity: 0.4
                        }}
                    />
                </div>

                <div className="genreContainer">
                    <IconButton
                        iconStyle={localStyles.genreIconStyle}
                        style={Object.assign({}, localStyles.genreIconButton, {
                            display: this.props.genres.rock ? 'inline' : 'none'
                        })}
                    >
                        <ActionPanTool className="vIcon"/>
                    </IconButton>
                    <IconButton
                        iconStyle={localStyles.genreIconStyle}
                        style={Object.assign({}, localStyles.genreIconButton, {
                            display: this.props.genres.classic ? 'inline' : 'none'
                        })}
                    >
                        <ActionStars className="vIcon"/>
                    </IconButton>
                    <IconButton
                        iconStyle={localStyles.genreIconStyle}
                        style={Object.assign({}, localStyles.genreIconButton, {
                            display: this.props.genres.rnb ? 'inline' : 'none'
                        })}
                    >
                        <ImageMusicNote className="vIcon"/>
                    </IconButton>
                    <IconButton
                        iconStyle={localStyles.genreIconStyle}
                        style={Object.assign({}, localStyles.genreIconButton, {
                            display: this.props.genres.acoustic ? 'inline' : 'none'
                        })}
                    >
                        <AVHearing className="vIcon"/>
                    </IconButton>
                </div>
            </div>
        );
    }
}

export default Page;