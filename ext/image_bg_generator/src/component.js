import React from 'react';
import ReactDOM from 'react-dom';
import Page from './page';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

export default (props) => {
    return (
        <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme, {userAgent: 'all'})}>
            <Page
                bg_image={props.bg_image}
                title={props.title}
                artist={props.artist}
                genres={props.genres}
            />
        </MuiThemeProvider>
    );
};