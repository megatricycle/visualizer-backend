import React from 'react';
import ReactDOM from 'react-dom';
import component from './component';
import { renderToString } from 'react-dom/server';

export default function renderComponent(props) {
    return renderToString(component(props));
}