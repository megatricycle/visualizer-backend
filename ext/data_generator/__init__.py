import sys
import sh
import os
import shutil
import wave
import struct
import numpy as np
import pyaudio
import wave
from pydub import AudioSegment

def convertToWav(id):
    output = '.py-analyze-tmp/{}/temp.wav'.format(id)

    dir = os.path.dirname(os.path.realpath(__file__))

    os.mkdir(dir + '/.py-analyze-tmp/{}'.format(id)); 

    sh.ffmpeg('-i', dir + '/../shared/' + id + '/sound.mp3', dir + '/' + output)

    # convert to mono
    mono = AudioSegment.from_wav(dir + '/'  + output)
    mono = mono.set_channels(1)

    mono_path = dir + '/' + output[0:-4]

    mono.export(mono_path, format='wav')

    return mono_path

def get_frequencies_bar(input_filename):
    # Open the wave file and get info
    wave_file = wave.open(input_filename, 'r')
    data_size = wave_file.getnframes()
    sample_rate = wave_file.getframerate()
    sample_width = wave_file.getsampwidth()
    duration = data_size / float(sample_rate)

    # Read in sample data
    sound_data = wave_file.readframes(data_size)

    # Close the file, as we don't need it any more
    wave_file.close()

    # Unpack the binary data into an array
    unpack_fmt = '{}h'.format(data_size)
    sound_data = struct.unpack(unpack_fmt, sound_data)

    # Process many samples
    fouriers_per_second = 24 # Frames per second
    fourier_spread = 1.0/fouriers_per_second
    fourier_width = fourier_spread
    fourier_width_index = fourier_width * float(sample_rate)

    length_to_process = int(duration)

    total_transforms = int(round(length_to_process * fouriers_per_second))
    fourier_spacing = round(fourier_spread * float(sample_rate))

    lastpoint=int(round(length_to_process*float(sample_rate)+fourier_width_index))-1

    sample_size = fourier_width_index
    freq = sample_rate / sample_size * np.arange(sample_size)

    def getBandWidth():
        return (2.0/sample_size) * (sample_rate / 2.0)

    def freqToIndex(f):
        # If f (frequency is lower than the bandwidth of spectrum[0]
        if f < getBandWidth()/2:
            return 0
        if f > (sample_rate / 2) - (getBandWidth() / 2):
            return sample_size -1
        fraction = float(f) / float(sample_rate)
        index = round(sample_size * fraction)
        return index

    fft_averages = []
    frequency_bin_data = []

    def average_fft_bands(fft_array):
        num_bands = 12 # The number of frequency bands (12 = 1 octave)
        del fft_averages[:]
        for band in range(0, num_bands):
            avg = 0.0

            if band == 0:
                lowFreq = int(0)
            else:
                lowFreq = int(int(sample_rate / 2) / float(2 ** (num_bands - band)))
            hiFreq = int((sample_rate / 2) / float(2 ** ((num_bands-1) - band)))
            lowBound = int(freqToIndex(lowFreq))
            hiBound = int(freqToIndex(hiFreq))
            for j in range(lowBound, hiBound):
                avg += fft_array[j]

            avg /= (hiBound - lowBound + 1)
            fft_averages.append(avg)

    for offset in range(0, total_transforms):
        start = int(offset * sample_size)
        end = int((offset * sample_size) + sample_size -1)

        sample_range = sound_data[start:end]
        ## FFT the data
        fft_data = abs(np.fft.fft(sample_range))
        # Normalise the data a second time, to make numbers sensible
        fft_data *= ((2**.5)/sample_size)
        average_fft_bands(fft_data)

        temp = list(fft_averages)

        frequency_bin_data.append(temp)

    return frequency_bin_data

def get_frequencies_pulse(input_filename):
    frequency_bin_data = get_frequencies_bar(input_filename)

    avg_frequency_bin_data = []

    for i in range(0, len(frequency_bin_data)):
        avg_frequency_bin_data.append(np.average(frequency_bin_data[i]))

    return avg_frequency_bin_data


def cleanTmp(id):
    dir = os.path.dirname(os.path.realpath(__file__))

    shutil.rmtree(dir + '/.py-analyze-tmp/{}'.format(id), ignore_errors = True)

def analyze(id, v_mode):
    # clear temp files to be sure
    cleanTmp(id)

    # convert input to wav
    input_converted = convertToWav(id)

    # analyze wav
    if v_mode == 'bar':
        output = get_frequencies_bar(input_converted)
    else:
        output = get_frequencies_pulse(input_converted)

    # delete temp files
    cleanTmp(id)

    return output